[GtkTemplate (ui = "/com/svineet/LazyTreeStore/resources/main-window.ui")]
public class MainWindow : Gtk.Window {
    [GtkChild]
    private Gtk.TreeView d_tree_view;

    [GtkChild]
    private Gtk.TextView d_source_text_view;

    private LazyTreeStore store;

    public MainWindow (string[] args) {
        this.setup_treeview(args);

        this.destroy.connect(Gtk.main_quit);
    }

    private void setup_treeview (string[] args) {
        this.store = new LazyTreeStore();

        d_tree_view.insert_column_with_attributes (-1, "Name", new Gtk.CellRendererText (), "text", 0, null);

        d_tree_view.model = store;
    }

    [GtkCallback]
    private void on_load_clicked () {
        stdout.printf("Load triggered\n");

        store = new LazyTreeStore();
        d_tree_view.model = store;

        string source_text = d_source_text_view.get_buffer().text;
        TreeNode root = new TreeNode.from_json(source_text);

        store.load_items_from_tree_root(root);
    }

    [GtkCallback]
    private void on_switch_clicked () {
        stdout.printf("Switch triggered\n");
    }
}
