public static int main (string[] args) {
    Gtk.init (ref args);

    var window = new MainWindow(args);
    window.show_all();

    Gtk.main();
    return 0;
}
