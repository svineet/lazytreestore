public class LazyTreeStore : Gtk.TreeStore {
    construct {
        set_column_types(new Type[] { typeof(string) });
    }

    class Item {
        public string data;
        public TreeNode parent;

        public Item(string data_, TreeNode? parent_) {
            this.data = data_; this.parent = parent_;
        }
    }

    public void load_items_from_tree_root(TreeNode root, Gtk.TreeIter? parent=null) {
        Gtk.TreeIter iter;
        append(out iter, parent);
        set(iter, 0, root.data, -1);

        // stdout.printf("At " + root.data + "\n");

        foreach(TreeNode child in root.get_children()) {
            load_items_from_tree_root(child, iter);
        }
    }
}

