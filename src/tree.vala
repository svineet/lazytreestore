public class TreeNode: GLib.Object {
    public string data;
    TreeNode[] children;

    public TreeNode(string data_) {
        this.data = data_;
    }

    public TreeNode.from_json(string json_data) {
        var parser = new Json.Parser ();

        try {
            parser.load_from_data (json_data);
            var obj = parser.get_root().get_object();

            var data = obj.get_string_member("data");
            var children = obj.get_array_member("children");

            this.data = data;
            this.children = {};

            foreach (var child_node in children.get_elements()) {
                this.children += new TreeNode.from_json_object(child_node);
            }
        } catch (Error e) {
            stdout.printf("Something went wrong parsing JSON for the tree\n");
        }
    }

    public TreeNode.from_json_object(Json.Node node) {
        var obj = node.get_object();

        var data = obj.get_string_member("data");
        var children = obj.get_array_member("children");

        this.data = data;
        this.children = {};

        foreach (var child_node in children.get_elements()) {
            this.children += new TreeNode.from_json_object(child_node);
        }
    }

    public TreeNode[] get_children() {
        return this.children;
    }

    /*
       Order of traversal: Root, Children
    */
    public delegate void PreorderWalkCallback(TreeNode node, TreeNode? parent);
    public void preorder_walk(PreorderWalkCallback walk_cb, TreeNode? parent=null) {
        walk_cb(this, parent);
        foreach (TreeNode child in this.children) {
            child.preorder_walk(walk_cb, this);
        }
    }
}

