Fast Tree Store
===============

A custom `Gtk.TreeStore` which implements the following features
1. Lazy loading
2. On minor change in tree structure, change the treestore in place,
instead of complete reload

Aim
---

1. Efficient initial loading of directory structure in `TreeStore`
2. On demand loading of subdirectories (lazy loading)
3. Implement `update_tree()` which does minimal work required to change to new
directory structure.
4. Write tests for lazy loading
5. Write tests for `update_tree()`

Design
------

Tree store and view widget.

A load button to load up some tree
- Maybe a directory tree or some random tree.
- `Tree` class that supports `get_children()` for each Node.


Building
--------

Currently we are using `meson` for building.

