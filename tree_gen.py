import typer
import json


def generate_tree_object(level: int, child_num: int, depth: int, width: int):
    obj = { "data": f"{level}-{child_num}", "children": [] }

    if depth <= 1: return obj
    for i in range(1, width+1):
        cur = generate_tree_object(level+1, i, depth-1, width)
        obj["children"].append(cur)

    return obj


def main(filename: str, depth: int, width: int):
    with open(filename, "w") as dest_file:
        obj = generate_tree_object(1, 1, depth, width)
        json.dump(obj, dest_file)


if __name__=="__main__":
    typer.run(main)

